The Towers on State provides students with luxury student apartments just minutes away from the University of Wisconsin (UW), Madison, WI. Indulge in both the luxury of modern student housing while having the convenience of renting next to the University of Wisconsin (UW-Madison).

Address: 502 N Frances St, Madison, WI 53703, USA

Phone: 608-257-0701

Website: https://www.dwellstudentmadison502.com
